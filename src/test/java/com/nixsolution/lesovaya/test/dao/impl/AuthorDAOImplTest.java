package com.nixsolution.lesovaya.test.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.nixsolutions.lesovaya.dao.IAuthorDAO;
import com.nixsolutions.lesovaya.entity.Author;

@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration("classpath:spring-app-context.xml")
public class AuthorDAOImplTest {
	
	@Autowired
	private IAuthorDAO authorDAO;
	
 
    @Test
    public void shouldFindAllAuthors() {
        List<Author> authors = authorDAO.findAll(); 
        assertEquals(10,authors.size());
    }
 
    @Test
    public void shouldCreateNewAuthor() {
    	 Author author = new Author();
         author.setFirstName("Martin");
         author.setMiddleName("A.A.");
         author.setLastName("Swong");
         authorDAO.create(author);
         Author savedAuthor = authorDAO.findById(11L);
         assertEquals("Martin", savedAuthor.getFirstName());
         assertEquals("A.A.", savedAuthor.getMiddleName());
         assertEquals("Swong", savedAuthor.getLastName());
    }
    
    @Test
    public void shouldDeleteAuthor() {
        Author author = authorDAO.findById(6L);
        authorDAO.delete(author);        
        assertNull(authorDAO.findById(6L));        
    }
    
    @Test
    public void shouldUpdateAuthor() {
    	  Author author = authorDAO.findById(2L);
    	  author.setMiddleName("midName");
          authorDAO.update(author);     
          Author testAuthor = authorDAO.findById(2L);
          assertEquals("midName", testAuthor.getMiddleName());
    }
    
    @Test
    public void shouldFindAuthorById() {
        Author author = authorDAO.findById(2L);
        assertEquals(author.getId(), new Integer(2));
        assertEquals("Connor", author.getFirstName());
    }
    
    @Test
    public void shouldFindByLastName() {
    	  List<Author> authors = authorDAO.findByLastName("Frankl");
    	  assertEquals(1,authors.size());
    }
    
    @Test
    public void testFindByFirstLetter() {
    	List<Author> authors = authorDAO.findByFirstLetter("L");
    	assertEquals(authors.get(0).getLastName(), "Lewis");
    }
 
}
