INSERT INTO book_category (name) VALUES ('adventure');
INSERT INTO book_category (name) VALUES ('biography');
INSERT INTO book_category (name) VALUES ('classics');
INSERT INTO book_category (name) VALUES ('fantasy');
INSERT INTO book_category (name) VALUES ('contemporary fiction');
INSERT INTO book_category (name) VALUES ('crime');

INSERT INTO client (first_name, last_name, phone) VALUES ('Addison', 'Smith', '12345');
INSERT INTO client (first_name, last_name, phone) VALUES ('Adele', 'Lam', '12345');
INSERT INTO client (first_name, last_name, phone) VALUES ('Benjamin', 'Taylor', '12345');
INSERT INTO client (first_name, last_name, phone) VALUES ('Edison', 'Anderson', '12345');
INSERT INTO client (first_name, last_name, phone) VALUES ('George', 'Chan', '12345');
INSERT INTO client (first_name, last_name, phone) VALUES ('Helen', 'Jones', '12345');
INSERT INTO client (first_name, last_name, phone) VALUES ('Camellia', 'Tremblay', '12345');
INSERT INTO client (first_name, last_name, phone) VALUES ('Dakota', 'Brown', '12345');
INSERT INTO client (first_name, last_name, phone) VALUES ('Ferdy', 'Martin', '12345');
INSERT INTO client (first_name, last_name, phone) VALUES ('Ben', 'Roy', '12345');
INSERT INTO client (first_name, last_name, phone) VALUES ('James', 'White', '12345');
INSERT INTO client (first_name, last_name, phone) VALUES ('Bennett', 'Campbell', '12345');

INSERT INTO author (first_name, middle_name, last_name) VALUES ('George', 'R.R.', 'Martin');
INSERT INTO author (first_name, last_name) VALUES ('Connor', 'Franta');
INSERT INTO author (first_name, last_name) VALUES ('Victor', 'Frankl');
INSERT INTO author (first_name, last_name) VALUES ('Jon', 'Krakauer');
INSERT INTO author (first_name, last_name) VALUES ('Damien', 'Lewis');
INSERT INTO author (first_name, last_name) VALUES ('Robert', 'Martin');
INSERT INTO author (first_name, last_name) VALUES ('Andrew', 'Hunt');
INSERT INTO author (first_name, last_name) VALUES ('Elisabeth', 'Freeman');
INSERT INTO author (first_name, last_name) VALUES ('Joshua', 'Bloch');
INSERT INTO author (first_name, last_name) VALUES ('Jon', 'Duckett');

INSERT INTO book (isbn,book_category_id, title, description) VALUES ('123456',1, 'A game of thrones', 'The first volume of A Song of Ice and Fire, the greatest fantasy epic of the modern age. GAME OF THRONES is now a major TV series from HBO, starring Sean Bean.');

INSERT INTO book_author(book_id, author_id) VALUES (1, 1);

INSERT INTO book (isbn, book_category_id, title, description) VALUES ('123457',5, 'A storm of swords', 'THE BOOK BEHIND THE THIRD SEASON OF GAME OF THRONES, AN ORIGINAL SERIES NOW ON HBO');

INSERT INTO book_author(book_id, author_id) VALUES (2, 1);

INSERT INTO book (isbn, book_category_id, title, description) VALUES ('123458',2, 'A work in progress', 'In this intimate memoir of life beyond the camera, Connor Franta shares the lessons he has learned on his journey from small-town boy to Internet sensation-so far.');

INSERT INTO book_author(book_id, author_id) VALUES (3, 2);

INSERT INTO book (isbn, book_category_id, title, description) VALUES ('123459', 3, 'Man search for meaning', 'With a new Foreword by Harold S. Kushner and a new Biographical Afterword by William J. Winslade Psychiatrist Viktor Frankl memoir has riveted generations of readers with its descriptions of life in Nazi death camps and its lessons for spiritual survival. Between 1942 and 1945 Frankl labored in four different camps, including Auschwitz, while his parents, brother, and pregnant wife perished.BOOK ');


INSERT INTO book_author(book_id, author_id) VALUES (4, 3);

INSERT INTO book (isbn, book_category_id, title) VALUES ('123450', 2, 'Into thin air: A personal account of the Everest disaster');

INSERT INTO book_author(book_id, author_id) VALUES (5, 4);

INSERT INTO book (isbn, book_category_id, title, description) VALUES ('1234501', 2, 'War Dog', 'In the winter of 1939 in the cold snow of no-mans-land, two loners met and began an extraordinary journey together, one that would bind them for the rest of their lives. One was an orphaned puppy, abandoned by his owners as they fled the approaching Nazi forces.');

INSERT INTO book_author(book_id, author_id) VALUES (6, 5);

INSERT INTO book (isbn, book_category_id, title) VALUES ('12345756', 2, 'Into the wild');

INSERT INTO book_author(book_id, author_id) VALUES (7, 4);

INSERT INTO book_client (book_id, client_id, datetime_loan) VALUES (1, 2, CURRENT_TIMESTAMP());

INSERT INTO book_client (book_id, client_id, datetime_loan) VALUES (2, 3, CURRENT_TIMESTAMP());

INSERT INTO book_client (book_id, client_id, datetime_loan) VALUES (3, 4, CURRENT_TIMESTAMP());

INSERT INTO book_client (book_id, client_id, datetime_loan) VALUES (4, 5, CURRENT_TIMESTAMP());

INSERT INTO book_client (book_id, client_id, datetime_loan) VALUES (7, 2, CURRENT_TIMESTAMP());

INSERT INTO book_client (book_id, client_id, datetime_loan) VALUES (6, 2, CURRENT_TIMESTAMP());

INSERT INTO book_cell (book_id, cell_number, bookcase_number, bookshelf_number) VALUES (1,1,1,1);
INSERT INTO book_cell (book_id, cell_number, bookcase_number, bookshelf_number) VALUES (2,2,1,1);
INSERT INTO book_cell (book_id, cell_number, bookcase_number, bookshelf_number) VALUES (3,3,1,1);
INSERT INTO book_cell (book_id, cell_number, bookcase_number, bookshelf_number) VALUES (4,4,1,1);
INSERT INTO book_cell (book_id, cell_number, bookcase_number, bookshelf_number) VALUES (5,5,1,1);
INSERT INTO book_cell (book_id, cell_number, bookcase_number, bookshelf_number) VALUES (6,6,1,1);
INSERT INTO book_cell (book_id, cell_number, bookcase_number, bookshelf_number) VALUES (7,7,1,1);

INSERT INTO role (name) VALUES ('ROLE_ADMIN');

INSERT INTO role (name) VALUES ('ROLE_LIBRARIAN');

INSERT INTO role (name) VALUES ('ROLE_CLIENT');

INSERT INTO user (first_name, last_name, login, password, role_id, enabled) VALUES ('Helen','Jones','admin','a', 1, true);
INSERT INTO user (first_name, last_name, login, password, role_id, enabled) VALUES ('Ben','Roy','libr', 'l', 2, true);
INSERT INTO user (first_name, last_name, login, password, role_id, enabled) VALUES ('Bennett','Campbell','client', 'c', 3, true);