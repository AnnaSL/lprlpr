var table = document.getElementById('list');
if (table) {
	table.onclick = function(event) {
		var target = event.target;
		if (target.tagName != "TD")
			return;
		if (target.parentNode.classList.contains("selected")) {
			target.parentNode.classList.remove("selected");
		} else {
			target.parentNode.classList.add("selected");
		}
	}
}

function tooltip(el, txt) {
	tipobj = document.getElementById('floatTip');
	tipobj.innerHTML = txt;
	moveTip(el);
	tipobj.style.display = "block";
}

function hide_info(el) {
	document.getElementById('floatTip').style.display = "none";
}

function moveTip(el) {
	floatTipStyle = document.getElementById("floatTip").style;
	w = 174;
	var x = el.offsetLeft;
	var y = el.offsetTop;
	var newX = x + w + 10 + 'px';
	var newY = y + 'px';
	floatTipStyle.left = newX;
	floatTipStyle.top = newY;
}

var REQURED_FIELD = {
	isValid : function(field) {
		return !!field && field.value.length !== 0;
	},
	message : "This field is required"
}

var ONLY_NUMBERS = {
	isValid : function(field) {
		var reg = /^\d+$/;
		return field.value.match(reg);
	},
	message : "This field should contains only numbers"
}

var ONLY_LETTERS = {
	isValid : function(field) {
		var reg = /^[a-zA-Z\s\.\-,!\:\?]+$/;
		return field.value.match(reg);
	},
	message : "This field should contains only letters and punctuation"
}

var ISBN_VALIDATOR = {
	isValid : function(field) {
		return field.value.length >= 10 && field.value.length <= 13;
	},
	message : "ISBN length should be from 10 to 13 numbers"
}

var MIDLLE_NAME_VALIDATOR = {
	isValid : function(field) {
		var reg = /^[a-zA-Z\s\.]+$/;
		var result = true;
		if (field.value.length !== 0 && !field.value.match(reg)) {
			result = false;
		}
		return result;
	},
	message : "This field should contains only letters"
}

function validate(form, options) {
	var result = true;
	for ( var property in options) {
		var arrayOfValidators = options[property];
		clearErrorMessages(property);
		for (var i = 0, l = arrayOfValidators.length; i < l; i++) {
			if (!arrayOfValidators[i].isValid(form.elements[property])) {
				clearErrorMessages(property);
				var para = document.createElement("P");
				para.classList.add("error-message");
				var t = document.createTextNode(arrayOfValidators[i].message);
				para.appendChild(t);
				form.elements[property].parentNode.appendChild(para);
				result = false;
			}
		}
	}
	return result;
}

function clearErrorMessages(property) {
	var errorElements = document.getElementById(property).parentNode
			.getElementsByClassName("error-message");
	if (errorElements.length != 0) {
		for (var j = 0, len = errorElements.length; j < len; j++) {
			errorElements[j].parentNode.removeChild(errorElements[j]);
		}
	}
}
