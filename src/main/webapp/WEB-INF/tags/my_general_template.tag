<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@attribute name="title"%>
<%@attribute name="head_area" fragment="true"%>
<%@attribute name="content_area" fragment="true"%>
<%@attribute name="leftsidebar_area" fragment="true" %>
<%@attribute name="header_area" fragment="true" %>
<c:url value="/resources/style/style.css" var="cssUrl"/>
<c:url value="/resources/scripts/script.js" var="scriptUrl"/>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>${title}</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link href="${cssUrl}" type="text/css" rel="stylesheet">	
	<jsp:invoke fragment="head_area" />
	<style>
		.selected {
			background: #D3D3D3;
		}
			
		tr {
			cursor: pointer;
		}
		#floatTip {
			position: absolute; 
			width: 250px; 
			display: none; 
			padding: 4px;
			font-family: sans-serif; 
			font-size: 9pt;
			color: #333; 
			background: #D3D3D3;
  	 	}
	</style>
	
</head>

<body>

<div class="wrapper">

	<header class="header">	
		<strong class="logo">
			<a href="#">Library</a>
		</strong>
		<div id="search">
			<form action="#" method="get">
				<input type="search" placeholder="search...">
			</form>
			<jsp:invoke fragment="header_area"></jsp:invoke>
		</div>
	</header><!-- .header-->

	<div class="middle"> 
		<div class="container"> 
			<div class="left-sidebar">
				<jsp:invoke fragment="leftsidebar_area"></jsp:invoke>
			</div>
		
			<div class="content">
				<jsp:invoke fragment="content_area" />
				<div id="floatTip"></div>
			</div>
		</div>				
	 </div>

</div>
<script src="${scriptUrl}" defer  type="text/javascript"></script>
<footer class="footer">
	<div class="copyright">
		<a href="#"> &copy 2015 Library. All Rights Reserved.</a>
	</div>
</footer>

</body>
</html>