<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<c:url value="/logout" var="logoutUrl"/> 
<t:my_general_template title="Welcome">
	<jsp:attribute name="header_area">
		<a href="${logoutUrl}">Logout</a>
	</jsp:attribute>
    <jsp:attribute name="content_area">
    	<h2>Welcome, client</h2>
    </jsp:attribute>
    <jsp:attribute name="leftsidebar_area">
    <c:url value="/homePage" var="homeUrl"/>  
		<ul>
			<li><a href="${homeUrl}">Home</a></li>
		</ul> 
    </jsp:attribute>
</t:my_general_template>