<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<c:url value="/login" var="loginUrl"/>  
<t:my_general_template title="Welcome">
    <jsp:attribute name="content_area">
    <c:if test="${not empty error}">
			<div><font color=red>${error}</font></div><br>
		</c:if>
		<c:if test="${not empty msg}">
			<div><font color=red>${msg}</font></div><br>
	</c:if>    
    <h2>Please, login to continue</h2> <br>		
		<form method="POST" action="${loginUrl}">
				
				<div style="margin:10px">
					<label>Login</label>
					<input type="text" name="username" onmouseover="tooltip(this,'Type in your username in this box')" onmouseout="hide_info(this)"/><br>
				</div>
				<div style="margin:10px">
					<label>Password</label>
					<input type="password" name="password" onmouseover="tooltip(this,'Type in your password in this box')" onmouseout="hide_info(this)"/><br>
				</div>
				<div style="margin:10px">
					<input type="submit" value="Login" />
					<input type="reset" value="Reset" />
				</div>
		</form>		
    </jsp:attribute>
</t:my_general_template>
