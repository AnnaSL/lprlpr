<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:url value="/logout" var="logoutUrl"/> 
<c:url value="/homePage" var="homeUrl"/>
<t:my_general_template title="Administrator page">
	<jsp:attribute name="header_area">
		<a href="${logoutUrl}">Logout</a>
	</jsp:attribute>
	<jsp:attribute name="content_area">
	<h2>Welcome, Administrator</h2>
	</jsp:attribute>
    <jsp:attribute name="leftsidebar_area">
    <c:url var="listCases" value="/case/list" ></c:url>
    <c:url var="listAuthor" value="/author/list" ></c:url>
    <c:url var="listCategory" value="/category/list" ></c:url>
    <c:url var="listBook" value="/book/list" ></c:url>
		<ul>
			<li><a href="${homeUrl}">Home</a></li>
			<li><a href="${listCases}">Storage</a></li>
			<li><a href="${listAuthor}">Author</a></li>
			<li><a href="${listCategory}">Category</a></li>
			<li><a href="${listBook}">Book</a></li>
		</ul> 
		<!--<p>${pageContext.request.userPrincipal.name}</p>  -->
    </jsp:attribute>
</t:my_general_template>


