<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page isELIgnored="false"%>
<c:url var="editAction" value="/author/edit"/>
<c:url var="deleteAction" value="/author/delete"/>
<c:url var="addAction" value="/author/add"/>
<c:url value="/resources/style/Editing Edit.png" var="editButton"/>
<c:url value="/resources/style/editing-delete-icon.png" var="deleteButton"/>
<c:url value="/logout" var="logoutUrl"/> 
<c:url value="/homePage" var="homeUrl"/>  
<t:my_general_template title="List of authors">
	<jsp:attribute name="header_area">
		<a href="${logoutUrl}">Logout</a>
	</jsp:attribute>
    <jsp:attribute name="content_area"> 
    <h2>List of authors</h2><br>      
	<div>
		<c:if test="${bookAssociated == 1}"><label><font color=red>This author associated with some book. You can't delete it.</font></label><br></c:if>
		<table class="myTable" id="list">
			<thead>
				<tr>
					<td>First name</td>
					<td>Middle name</td>
					<td>Last name</td>
					<td colspan=2>Action</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${authors}" var="author">
					<tr>
						<td><c:out value="${author.firstName}"></c:out></td>
						<td><c:out value="${author.middleName}"></c:out></td>
						<td><c:out value="${author.lastName}"></c:out></td>
						<td><form action="${editAction}" method="get">
								<input type="hidden" name="authorid"
									value=<c:out value="${author.id}"></c:out> /> 
									<input class = "editDelete" type="image" src="${editButton}"/>
							</form></td>
						<td><form action="${deleteAction}" method="post">
								<input type="hidden" name="authorid"
									value=<c:out value="${author.id}"></c:out> />
								<input type="image" class = "editDelete" src="${deleteButton}"/>
							</form></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<p>
		<form action="${addAction}" method="get">
			<input class="add" type="submit" value="New author"/>
		</form>
		</p>
	</div>
    </jsp:attribute>
    <jsp:attribute name="leftsidebar_area">
	    <c:url var="listCases" value="/case/list" ></c:url>
	    <c:url var="listAuthor" value="/author/list" ></c:url>
	    <c:url var="listCategory" value="/category/list" ></c:url>
	    <c:url var="listBook" value="/book/list" ></c:url>
		<ul>
			<li><a href="${homeUrl}">Home</a></li>
			<li><a href="${listCases}">Storage</a></li>
			<li><a href="${listAuthor}">Author</a></li>
			<li><a href="${listCategory}">Category</a></li>
			<li><a href="${listBook}">Book</a></li>
		</ul> 
    </jsp:attribute>
</t:my_general_template>