<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored ="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<c:url var="saveAction" value="/author/save"/>
<c:url value="/logout" var="logoutUrl"/>
<c:url value="/homePage" var="homeUrl"/>  
<t:my_general_template title="Add/Edit author">
	<jsp:attribute name="header_area">
		<a href="${logoutUrl}">Logout</a>
	</jsp:attribute>
    <jsp:attribute name="content_area">
    	<h2>Add/Edit author</h2><br>
        <form:form action="${saveAction}" commandName="author" method="post" onsubmit="return validate(this, {firstName: [ONLY_LETTERS,REQURED_FIELD], middleName: [MIDLLE_NAME_VALIDATOR], lastName: [ONLY_LETTERS,REQURED_FIELD]})">
				<label for="firstName">First name</label><br>
				<div><form:input path="firstName" onmouseover="tooltip(this,'Type in first name in this box')" onmouseout="hide_info(this)"/></div>
				<label for="middleName">Middle name</label><br>
				<div><form:input path="middleName" onmouseover="tooltip(this,'Type in middle name in this box')" onmouseout="hide_info(this)"/></div>
				<label for="lastName">Last name</label><br>
				<div><form:input path="lastName" onmouseover="tooltip(this,'Type in last name in this box')" onmouseout="hide_info(this)"/></div>
				<form:hidden path = "id"/>
				<br><input type="submit" class="add" value="Save">
				<input class="add" type="reset" value="Reset">
		</form:form>
    </jsp:attribute>
     <jsp:attribute name="leftsidebar_area">
		<c:url var="listCases" value="/case/list" ></c:url>
    	<c:url var="listAuthor" value="/author/list" ></c:url>
    	<c:url var="listCategory" value="/category/list" ></c:url>
    	<c:url var="listBook" value="/book/list" ></c:url>
		<ul>
			<li><a href="${homeUrl}">Home</a></li>
			<li><a href="${listCases}">Storage</a></li>
			<li><a href="${listAuthor}">Author</a></li>
			<li><a href="${listCategory}">Category</a></li>
			<li><a href="${listBook}">Book</a></li>
		</ul> 
    </jsp:attribute>
</t:my_general_template>