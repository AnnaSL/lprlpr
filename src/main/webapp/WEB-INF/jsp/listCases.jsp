<%@page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<c:url value="/resources/style/Editing Edit.png" var="viewButton"/>
<c:url value="/resources/style/editing-delete-icon.png" var="deleteButton"/>
<c:url value="/logout" var="logoutUrl"/> 
<c:url value="/homePage" var="homeUrl"/>  
<t:my_general_template title="List of cases">
	<jsp:attribute name="header_area">
		<a href="${logoutUrl}">Logout</a>
	</jsp:attribute>
    <jsp:attribute name="content_area">
    <h2>List of cases</h2><br>     
    <c:if test="${caseNotEmpty == 1}"><label><font color=red>Case isn't empty. You can't delete it.</font></label><br></c:if>
    	<c:url var="viewAction" value="/case/view" ></c:url>
    	<c:url var="deleteAction" value="/case/delete" ></c:url>
    	<c:url var="addAction" value="/case/add" ></c:url>
    	<div>    	
			<table class="myTable" id="list">
				<thead>
					<tr>
						<td>Case</td>
						<td colspan = "2">Action</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${cases}" var="cases">
						<tr>
							<td><c:out value="${cases}"></c:out></td>
							 <td> <form name="view" action="${viewAction}" method="get">
								<input type="hidden" name="casenum" value=<c:out value="${cases}" />>
								<input class = "editDelete" type="image" src="${viewButton}"/>
								</form>
							</td>
							<td> <form name="delete" action="${deleteAction}" method="post">
								<input type="hidden" name="casenum" value=<c:out value="${cases}" />>
								<input type="image" class = "editDelete" src="${deleteButton}"/>
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<p>
			<form action="${addAction}" method="get">
				<input class="add" type="submit" name="action" value="New case" />
			</form>
			</p>
		</div>
    </jsp:attribute>
     <jsp:attribute name="leftsidebar_area">
		<c:url var="listCases" value="/case/list" ></c:url>
    	<c:url var="listAuthor" value="/author/list" ></c:url>
    	<c:url var="listCategory" value="/category/list" ></c:url>
    	<c:url var="listBook" value="/book/list" ></c:url>
		<ul>
			<li><a href="${homeUrl}">Home</a></li>
			<li><a href="${listCases}">Storage</a></li>
			<li><a href="${listAuthor}">Author</a></li>
			<li><a href="${listCategory}">Category</a></li>
			<li><a href="${listBook}">Book</a></li>
		</ul> 
    </jsp:attribute>
</t:my_general_template>