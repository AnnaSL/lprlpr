<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page isELIgnored ="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<c:url var="editAction" value="/category/edit"/>
<c:url var="deleteAction" value="/category/delete"/>
<c:url var="addAction" value="/category/add"/>
<c:url value="/resources/style/Editing Edit.png" var="editButton"/>
<c:url value="/resources/style/editing-delete-icon.png" var="deleteButton"/>
<c:url value="/logout" var="logoutUrl"/> 
<c:url value="/homePage" var="homeUrl"/>  
<t:my_general_template title="List of categories">
	<jsp:attribute name="header_area">
		<a href="${logoutUrl}">Logout</a>
	</jsp:attribute>
    <jsp:attribute name="content_area">
    <h2>List of categories</h2><br>
    <c:if test="${categoryAssociated == 1}"><label><font color=red>This category associated with some book. You can't delete it.</font></label><br></c:if>
	         <table class="myTable" id="list">
		        <thead>
		            <tr>
		                <th>Category name</th>
		                <th>Description</th>
		                <th colspan=2>Action</th>
		            </tr>
		        </thead>
		        <tbody>
		            <c:forEach items="${categories}" var="category">
		                <tr>
		                    <td><c:out value="${category.name}" /></td>
		                    <td><c:out value="${category.description}" /></td>
		                    <td> <form name="edit" action="${editAction}" method="GET">
								<input type="hidden" name="categoryid" value=<c:out value="${category.id}" />>
								<input class = "editDelete" type="image" src="${editButton}" />
								</form>
							</td>
							<td> <form name="delete" action="${deleteAction}" method="POST">
								<input type="hidden" name="categoryid" value=<c:out value="${category.id}" />>
								<input type="image" class = "editDelete" src="${deleteButton}" />
								</form>
							</td>
		                </tr>
		            </c:forEach>
		        </tbody>
	    </table>
	    <p><form name="add" action="${addAction}" method="GET">
	    	<input class="add" type="submit" name="action" value="New category">
	    	</form>
	    </p>
    </jsp:attribute>
    <jsp:attribute name="leftsidebar_area">
		<c:url var="listCases" value="/case/list" ></c:url>
	    <c:url var="listAuthor" value="/author/list" ></c:url>
	    <c:url var="listCategory" value="/category/list" ></c:url>
	    <c:url var="listBook" value="/book/list" ></c:url>
		<ul>
			<li><a href="${homeUrl}">Home</a></li>
			<li><a href="${listCases}">Storage</a></li>
			<li><a href="${listAuthor}">Author</a></li>
			<li><a href="${listCategory}">Category</a></li>
			<li><a href="${listBook}">Book</a></li>
		</ul> 
    </jsp:attribute>
</t:my_general_template>