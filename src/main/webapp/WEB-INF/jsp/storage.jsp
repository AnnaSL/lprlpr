<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<c:url value="/logout" var="logoutUrl"/>  
<c:url value="/homePage" var="homeUrl"/>
<t:my_general_template title="Add/Edit storage">
	<jsp:attribute name="header_area">
		<a href="${logoutUrl}">Logout</a>
	</jsp:attribute>
    <jsp:attribute name="content_area">
    <c:url var="saveCase" value="/case/save" ></c:url>
    	<h2>Add/Edit case</h2><br>   
        <div>
			<form action="${saveCase}" method="post"  onsubmit="return validate(this, {bookCaseNumber: [ONLY_NUMBERS,REQURED_FIELD], bookShelfNumber: [ONLY_NUMBERS,REQURED_FIELD], cellNumber: [ONLY_NUMBERS,REQURED_FIELD]})">
				<label for="bookCaseNumber">Case number</label><br>
				<div><input type="text" name="bookCaseNumber" id="bookCaseNumber" value="<c:out value="${maxcasenum}"/>"></div> 
				<label for="bookShelfNumber">Shelf number</label><br>
				<div><input type="text" name="bookShelfNumber" id="bookShelfNumber" onmouseover="tooltip(this,'Type in book shelf number in this box')" onmouseout="hide_info(this)"/></div>
				<label for="cellNumber">Cell number per shelf</label><br>
				<div><input type="text" name="cellNumber" id="cellNumber" onmouseover="tooltip(this,'Type in cell number per shelf in this box')" onmouseout="hide_info(this)"/></div>
				<input class="add" type="submit" name="action" value="save"> 
				<input class="add" type="reset" value="Reset">
			</form>
		</div>	
    </jsp:attribute>
    <jsp:attribute name="leftsidebar_area">
		<c:url var="listCases" value="/case/list" ></c:url>
	    <c:url var="listAuthor" value="/author/list" ></c:url>
	    <c:url var="listCategory" value="/category/list" ></c:url>
	    <c:url var="listBook" value="/book/list" ></c:url>
		<ul>
			<li><a href="${homeUrl}">Home</a></li>
			<li><a href="${listCases}">Storage</a></li>
			<li><a href="${listAuthor}">Author</a></li>
			<li><a href="${listCategory}">Category</a></li>
			<li><a href="${listBook}">Book</a></li>
		</ul> 
    </jsp:attribute>
</t:my_general_template>