<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored ="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<c:url var="saveAction" value="/category/save"/>
<c:url value="/logout" var="logoutUrl"/> 
<c:url value="/homePage" var="homeUrl"/>  
<t:my_general_template title="Add/Edit category">
	<jsp:attribute name="header_area">
		<a href="${logoutUrl}">Logout</a>
	</jsp:attribute>
    <jsp:attribute name="content_area">
    	<h2>Add/Edit category</h2><br>
        <form:form action="${saveAction}" commandName="category" method="post" onsubmit="return validate(this, {name: [ONLY_LETTERS,REQURED_FIELD]})">
				<label for="name">Category name</label><br>
				<div><form:input path="name" onmouseover="tooltip(this,'Type in category name in this box')" onmouseout="hide_info(this)"/></div>
				<label for="description">Description</label><br>
				<div><form:input path="description" onmouseover="tooltip(this,'Type in category description in this box')" onmouseout="hide_info(this)"/></div>
				<form:hidden path = "id"/>
				<input class="add" type="submit" value="Save">
				<input class="add" type="reset" value="Reset">
		</form:form>
    </jsp:attribute>
    <jsp:attribute name="leftsidebar_area">
	    <c:url var="listCases" value="/case/list" ></c:url>
	    <c:url var="listAuthor" value="/author/list" ></c:url>
	    <c:url var="listCategory" value="/category/list" ></c:url>
	    <c:url var="listBook" value="/book/list" ></c:url>
		<ul>
			<li><a href="${homeUrl}">Home</a></li>
			<li><a href="${listCases}">Storage</a></li>
			<li><a href="${listAuthor}">Author</a></li>
			<li><a href="${listCategory}">Category</a></li>
			<li><a href="${listBook}">Book</a></li>
		</ul> 
    </jsp:attribute>
</t:my_general_template>
