<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored ="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="f" uri="http://nixsolutions.lesovaya.tld.com/functions" %>
<c:url var="saveAction" value="/book/save"></c:url>
<c:url value="/logout" var="logoutUrl"/>
<c:url value="/homePage" var="homeUrl"/>  
<t:my_general_template title="Add/Edit book">
	<jsp:attribute name="header_area">
		<a href="${logoutUrl}">Logout</a>
	</jsp:attribute>
    <jsp:attribute name="content_area">
    	<h2>Add/Edit book</h2><br>
		<form action="${saveAction}" method="post" onsubmit="return validate(this, {isbn: [ONLY_NUMBERS,ISBN_VALIDATOR,REQURED_FIELD], title: [ONLY_LETTERS,REQURED_FIELD], category: [REQURED_FIELD], authorsIds: [REQURED_FIELD]})">
			<c:if test="${not empty existIsbn}"><label><font color=red>ISBN exists in database.Check you ISBN or input other.</font></label><br></c:if>
			<label for="isbn" class="booklabel">ISBN</label><br><div><input ${editbook == 1 ? 'readonly' : ''} type="text" name="isbn" id="isbn" value="<c:out value="${book.isbn}"/>" onmouseover="tooltip(this,'Type in ISBN in this box')" onmouseout="hide_info(this)"/></div>
			<label for="title" class="booklabel">Title</label><br><div><input type="text" name="title" id="title" value="<c:out value="${book.title}"/>"onmouseover="tooltip(this,'Type in title in this box')" onmouseout="hide_info(this)"/></div>
			<label for="category">Category</label><br>
			<div>
			<select name="category" id="category" onmouseover="tooltip(this,'Select category for book')" onmouseout="hide_info(this)">
				<c:forEach items="${categoryList}" var="category">				
						<option value="${category.id}" ${category.id == categorySelected ? 'selected' : ''}>${category.name}</option>				
				</c:forEach>
			</select>
			</div>
			<label for="description" class="booklabel">Description</label><br>
			<div><input type="text"  name="description" value="<c:out value="${book.description}"/>"onmouseover="tooltip(this,'Type in description in this box')" onmouseout="hide_info(this)"/></div>
	   		<c:choose>
	   			<c:when test="${not empty editbook}">
	   				<label for="curcellnum" class="booklabel">Current cell number</label><br><div><input readonly type="text" name="curcellnum" value="<c:out value="${book.cellNumber}"/>"></div>
			   		<input type="hidden" name="curcellid" value="<c:out value="${book.cellId}"/>">   		
			   		<label for="cellcheck" class="booklabel">Change current cell</label><input type="checkbox" name="cellcheck" id="cellcheck" value="1">
					<input type="hidden" name="afterAction" value="afterEdit">
					<div>
						<select name="cellNum" id="cellNum" onmouseover="tooltip(this,'Select empty cell in this box')" onmouseout="hide_info(this)">
							<c:forEach items="${cellNumList}" var="cell">				
									<option value="${cell.id}">${cell.bookCaseNumber}.${cell.bookShelfNumber}.${cell.cellNumber}</option>				
							</c:forEach>
						</select>
					</div>		
	   			</c:when>
	   			<c:otherwise>
	   				<label for="cellNum">Empty cells</label> <br>
	   				<div>
						<select required name="cellNum" id="cellNum" onmouseover="tooltip(this,'Select empty cell in this box')" onmouseout="hide_info(this)">
							<c:forEach items="${cellNumList}" var="cell">				
									<option value="${cell.id}">${cell.bookCaseNumber}.${cell.bookShelfNumber}.${cell.cellNumber}</option>				
							</c:forEach>
						</select>
					</div>	
	   			</c:otherwise>
	   		</c:choose>   		
		<label class="booklabel">Author(s)</label>
		<div>
			<c:choose>
				<c:when test="${not empty editbook}">
					<select multiple name="authorsIds" id="authorsIds" size="7" onmouseover="tooltip(this,'Select one or many author(s) in this box')" onmouseout="hide_info(this)">
						<option>Choose author(s)</option>						
						<c:forEach items="${authorList}" var="author">
                                <c:set var="selected" value="false" />
                                <c:forEach var="authorByBook" items="${book.authors}">
                                    <c:if test="${author.id == authorByBook.id}">
                                        <c:set var="selected" value="true" />
                                    </c:if>
                                </c:forEach>
                                <c:if test="${selected}">
                                    <option value="${author.id}" selected>${author.lastName} ${author.middleName} ${author.firstName}</option>
                                </c:if>
                                <c:if test="${!selected}">
                                    <option value="${author.id}">${author.lastName} ${author.middleName} ${author.firstName}</option>
                                </c:if>
                         </c:forEach>	
					</select>	
				</c:when>
				<c:otherwise>
					<select multiple name="authorsIds" id="authorsIds" size="7" onmouseover="tooltip(this,'Select one or many author(s) in this box')" onmouseout="hide_info(this)">
						<option>Choose author(s)</option>
						<c:forEach items="${authorList}" var="author">
							<option value="${author.id}">${author.lastName} ${author.middleName} ${author.firstName}</option>			
						</c:forEach>			
					</select>	
				</c:otherwise>
			</c:choose><br>
		</div>
		<input type="hidden" name = "bookid" value="<c:out value="${book.id}"/>">
			<input class="add" type="submit" name = "action" value="Save">
			<input class="add" type="reset" value="Reset">
		</form>
    </jsp:attribute>
    <jsp:attribute name="leftsidebar_area">
	    <c:url var="listCases" value="/case/list" ></c:url>
	    <c:url var="listAuthor" value="/author/list" ></c:url>
	    <c:url var="listCategory" value="/category/list" ></c:url>
	    <c:url var="listBook" value="/book/list" ></c:url>
		<ul>
			<li><a href="${homeUrl}">Home</a></li>
			<li><a href="${listCases}">Storage</a></li>
			<li><a href="${listAuthor}">Author</a></li>
			<li><a href="${listCategory}">Category</a></li>
			<li><a href="${listBook}">Book</a></li>
		</ul> 
    </jsp:attribute>
</t:my_general_template>