<%@page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<c:url value="/logout" var="logoutUrl"/> 
<c:url value="/homePage" var="homeUrl"/>  
<t:my_general_template title="List of storages">
	<jsp:attribute name="header_area">
		<a href="${logoutUrl}">Logout</a>
	</jsp:attribute>
    <jsp:attribute name="content_area">
    <c:url var="filterCells" value="/case/filterCells" ></c:url>
    	<h2>List of storages</h2><br>   
	    <div>
	    	<form action="${filterCells}" method="get">
				<label>Fullness of cell</label>
				<select name="fullness">
					<option value="busy">busy cell</option>
					<option value="empty">empty cell</option>
				</select>
				<input type="hidden" value="${caseNum}" name="casenumFilter">
				<input class="add" type="submit" name = "action" value="Filter"/>
	    	</form>
		</div>
		<div>
			<table class="myTable" id="list">
				<thead>
					<tr>
						<td>Case</td>
						<td>Shelf</td>
						<td>Cell</td>
						<td>Status</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${storage}" var="storage">
						<tr>
							<td><c:out value="${storage.bookCaseNumber}"></c:out></td>
							<td><c:out value="${storage.bookShelfNumber}"></c:out></td>
							<td><c:out value="${storage.cellNumber}"></c:out></td>	
							<td><c:out value="${storage.book  == null ? 'cell empty' : 'busy' }"/> </td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
    </jsp:attribute>
     <jsp:attribute name="leftsidebar_area">
		<c:url var="listCases" value="/case/list" ></c:url>
	    <c:url var="listAuthor" value="/author/list" ></c:url>
	    <c:url var="listCategory" value="/category/list" ></c:url>
	    <c:url var="listBook" value="/book/list" ></c:url>
		<ul>
			<li><a href="${homeUrl}">Home</a></li>
			<li><a href="${listCases}">Storage</a></li>
			<li><a href="${listAuthor}">Author</a></li>
			<li><a href="${listCategory}">Category</a></li>
			<li><a href="${listBook}">Book</a></li>
		</ul> 
    </jsp:attribute>
</t:my_general_template>