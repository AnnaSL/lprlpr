package com.nixsolutions.lesovaya.entity;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "book_cell")
public class BookCell implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "book_cell_id")
	private Long id;
	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name = "book_id")
	private Book book;
	@Column(name = "cell_number")
	@NotNull
	private int cellNumber;
	@Column(name = "bookcase_number")
	@NotNull
	private int bookCaseNumber;
	@Column(name = "bookshelf_number")
	@NotNull
	private int bookShelfNumber;
	@Transient
	private String cellNumberConcated;

	public BookCell(){}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCellNumber() {
		return cellNumber;
	}

	public void setCellNumber(int cellNumber) {
		this.cellNumber = cellNumber;
	}

	public int getBookCaseNumber() {
		return bookCaseNumber;
	}

	public void setBookCaseNumber(int bookCaseNumber) {
		this.bookCaseNumber = bookCaseNumber;
	}

	public int getBookShelfNumber() {
		return bookShelfNumber;
	}

	public void setBookShelfNumber(int bookShelfNumber) {
		this.bookShelfNumber = bookShelfNumber;
	}

	public String getCellNumberConcated() {
		return cellNumberConcated;
	}

	public void setCellNumberConcated(String cellNumberConcated) {
		this.cellNumberConcated = cellNumberConcated;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}
	
	@Override
	public String toString() {
		return "BookCell [id=" + id + ", book=" + book + ", cellNumber=" + cellNumber + ", bookCaseNumber="
				+ bookCaseNumber + ", bookShelfNumber=" + bookShelfNumber + ", cellNumberConcated=" + cellNumberConcated
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookCell other = (BookCell) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
