package com.nixsolutions.lesovaya.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "book_client")
public class BookClient implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "book_client_id")
	private Long id;
	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name = "book_id")
	private Book book;
	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name = "client_id")
	private Client client;
	@Column(name = "datetime_loan")
	@NotBlank
	private Date datetimeLoan;
	@Column(name = "datetime_return")
	private Date datetimeReturn;

	public BookClient() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatetimeLoan() {
		return datetimeLoan;
	}

	public void setDatetimeLoan(Date datetimeLoan) {
		this.datetimeLoan = datetimeLoan;
	}

	public Date getDatetimeReturn() {
		return datetimeReturn;
	}

	public void setDatetimeReturn(Date datetimeReturn) {
		this.datetimeReturn = datetimeReturn;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return "BookClient [id=" + id + ", book=" + book + ", client=" + client + ", datetimeLoan=" + datetimeLoan
				+ ", datetimeReturn=" + datetimeReturn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookClient other = (BookClient) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
