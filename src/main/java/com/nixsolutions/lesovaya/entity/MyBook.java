package com.nixsolutions.lesovaya.entity;

import java.io.Serializable;

public class MyBook implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String isbn;
	private long id;
	private String title;
	private String categoryName;
	private long categoryId;
	private String description;	
	private String cellNumber;
	private int caseNumber;
	private int shelfNumber;
	private int cell;
	private long cellId;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(int caseNumber) {
		this.caseNumber = caseNumber;
	}
	public int getShelfNumber() {
		return shelfNumber;
	}
	public void setShelfNumber(int shelfNumber) {
		this.shelfNumber = shelfNumber;
	}
	public int getCell() {
		return cell;
	}
	public void setCell(int cell) {
		this.cell = cell;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long l) {
		this.categoryId = l;
	}
	public long getCellId() {
		return cellId;
	}
	public void setCellId(long l) {
		this.cellId = l;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public long getId() {
		return id;
	}
	public void setId(long bookId) {
		this.id = bookId;
	}
}
