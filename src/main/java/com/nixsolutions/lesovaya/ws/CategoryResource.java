package com.nixsolutions.lesovaya.ws;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.nixsolutions.lesovaya.entity.BookCategory;
import com.nixsolutions.lesovaya.services.IBookCategoryService;

@Component
@Path("/category")
public class CategoryResource {
	
	@Autowired(required=true)
	@Qualifier(value="iBookCategoryService")
	private IBookCategoryService bookCategoryService; 
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BookCategory> getAllCtegory() {		
		return bookCategoryService.findAll(); 
	}
	
	@GET
	@Path("/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public BookCategory getCategory(@PathParam("param") String id) {
		BookCategory category = bookCategoryService.findById(Integer.parseInt(id));		
		return category; 
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public String addCategory(@QueryParam("categoryName") String name) {
		BookCategory category = new BookCategory();
		category.setName(name);
		bookCategoryService.save(category);
	    return null;
	  }

	 @POST
	 @Produces(MediaType.APPLICATION_JSON)
	 public String updateCategory(@QueryParam("categoryName") String name, @QueryParam("id") String id) {
		 BookCategory category = new BookCategory();
		 category.setName(name);
		 category.setId(Long.parseLong(id));
		 bookCategoryService.save(category);
		 return null;
	 }

	  @DELETE
	  @Path("/{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public boolean deleteCategory(@PathParam("id") Long id) {
	    return bookCategoryService.delete(id);
	  }
}
