package com.nixsolutions.lesovaya.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.nixsolutions.lesovaya.dao.IRoleDAO;
import com.nixsolutions.lesovaya.entity.User;
import com.nixsolutions.lesovaya.services.IUserService;

@Endpoint
public class UserResource {

	private static final String NAMESPACE_URI = "http://nixsolutions.com/lesovaya/ws";
	@Autowired(required = true)
	@Qualifier(value = "iUserService")
	private IUserService userService;
	@Autowired(required = true)
	@Qualifier(value = "iRoleDAO")
	private IRoleDAO roleDao;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "userCreateRequest")
	@ResponsePayload
	public UserCreateResponse handleUserCreateRequest(@RequestPayload UserCreateRequest request) {

		UserCreateResponse response = new UserCreateResponse();
		com.nixsolutions.lesovaya.entity.Role role = roleDao.findByName(request.getRole());
		String login = request.getUsername();
		String password = request.getPassword();
		String firstName = request.getFirstname();
		String lastname = request.getLastname();		

		userService.save(firstName, "", lastname, login, password, "", role);
		User responseUser = userService.findByLoginName(login);
		response.setId(responseUser.getId());
		response.setUsername(responseUser.getLogin());
		response.setPassword(responseUser.getPassword());
		response.setRole(responseUser.getRole().getName());
		response.setFirstname(responseUser.getFirstName());
		response.setLastname(responseUser.getLastName());
		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "userUpdateRequest")
	@ResponsePayload
	public UserUpdateResponse handleUserUpdateRequest(@RequestPayload UserUpdateRequest request) {

		UserUpdateResponse response = new UserUpdateResponse();
		com.nixsolutions.lesovaya.entity.Role role = roleDao.findByName(request.getRole());
		String login = request.getUsername();
		String password = request.getPassword();
		Long id = request.getId();
		String firstName = request.getFirstname();
		String lastname = request.getLastname();		

		userService.save(firstName, "", lastname, login, password, id.toString(), role);
		User responseUser = userService.findByLoginName(login);
		response.setId(responseUser.getId());
		response.setUsername(responseUser.getLogin());
		response.setPassword(responseUser.getPassword());
		response.setRole(responseUser.getRole().getName());
		response.setFirstname(responseUser.getFirstName());
		response.setLastname(responseUser.getLastName());
		return response;
	}
}
