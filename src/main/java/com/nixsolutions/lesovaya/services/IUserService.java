package com.nixsolutions.lesovaya.services;

import com.nixsolutions.lesovaya.entity.Role;
import com.nixsolutions.lesovaya.entity.User;

public interface IUserService {
	
	void save(String firstName, String middleName, String lastName, String login, String password, String userId, Role role);
	boolean delete(Long id);
	User check(String login, String password);
	boolean update(Long id);
	User findByLoginName(String login);
}
