package com.nixsolutions.lesovaya.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.nixsolutions.lesovaya.dao.IBookCategoryDAO;
import com.nixsolutions.lesovaya.dao.IBookDAO;
import com.nixsolutions.lesovaya.entity.BookCategory;
import com.nixsolutions.lesovaya.services.IBookCategoryService;

//@Transactional
public class BookCategoryService implements  IBookCategoryService{
	
	@Autowired
	@Qualifier("iBookCategoryDao")
	IBookCategoryDAO bookCategoryDao;
	@Autowired
	@Qualifier("iBookDao")
	IBookDAO bookDao;
	
	public List<BookCategory> findAll() {		
		return bookCategoryDao.findAll();
	}

	public BookCategory findById(long id) {
		return bookCategoryDao.findById(id);
	}

	public void save(BookCategory category) {
		if (category.getId() == null) {
			bookCategoryDao.create(category);
		} else {
			bookCategoryDao.update(category);
		}		
	}

	public boolean delete(Long categoryId) {
		boolean result = false;
		BookCategory category = new BookCategory();
		if (bookDao.findCountBooksByCategory(categoryId) == 0) {
			category.setId((long) categoryId);
			bookCategoryDao.delete(category);
			result = true;
		} 
		return result;
	}

	public IBookCategoryDAO getBookCategoryDao() {
		return bookCategoryDao;
	}

	public void setBookCategoryDao(IBookCategoryDAO bookCategoryDao) {
		this.bookCategoryDao = bookCategoryDao;
	}

}
