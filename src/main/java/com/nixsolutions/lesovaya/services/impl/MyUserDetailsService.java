package com.nixsolutions.lesovaya.services.impl;

import java.util.ArrayList;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import com.nixsolutions.lesovaya.dao.IUserDAO;

//@Transactional
public class MyUserDetailsService implements UserDetailsService {

	private static Logger log = LogManager.getLogger(MyUserDetailsService.class.getName());
	private IUserDAO userDao;

	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		com.nixsolutions.lesovaya.entity.User user = null;
		List<GrantedAuthority> authorities = null;
		user = userDao.findByLoginName(username);
		if (user != null) {
			authorities = buildUserAuthority(user.getRole().getName());
		} else {
			log.error("UsernameNotFoundException: username = " + username);
			throw new UsernameNotFoundException(username + " not found");
		}
		return buildUserForAuthentication(user, authorities);
	}

	private User buildUserForAuthentication(com.nixsolutions.lesovaya.entity.User user,
			List<GrantedAuthority> authorities) {
		return new User(user.getLogin(), user.getPassword().toLowerCase(), true, true, true, true, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(String role) {
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
		setAuths.add(new SimpleGrantedAuthority(role));
		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

		return Result;
	}

	public IUserDAO getUserDao() {
		return userDao;
	}

	public void setUserDao(IUserDAO userDao) {
		this.userDao = userDao;
	}

}
