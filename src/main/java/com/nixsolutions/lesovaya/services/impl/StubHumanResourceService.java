package com.nixsolutions.lesovaya.services.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.nixsolutions.lesovaya.services.HumanResourceService;

//@Service
public class StubHumanResourceService implements HumanResourceService {
	public void bookHoliday(Date startDate, Date endDate, String name) {
        System.out.println("Booking holiday for [" + startDate + "-" + endDate + "] for [" + name + "] ");
    }
}
