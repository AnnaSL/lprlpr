package com.nixsolutions.lesovaya.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import com.nixsolutions.lesovaya.dao.IUserDAO;
import com.nixsolutions.lesovaya.entity.Role;
import com.nixsolutions.lesovaya.entity.User;
import com.nixsolutions.lesovaya.services.IUserService;

//@Transactional
public class UserService implements IUserService {
	@Autowired
	@Qualifier("iUserDAO")
	private IUserDAO userDao;

	@Override
	public User check(String login, String password) {

		User user = new User();
		if (userDao.findByLoginAndPassword(login, password)) {
			user = userDao.findByLoginName(login);
		} else {
			user.setLogin("notfound");
		}
		return user;
	}

	@Override
	public boolean delete(Long id) {
		User userDel = new User();
		userDel.setId(id);
		userDao.delete(userDel);
		return true;
	}

	@Override
	public void save(String firstName, String middleName, String lastName, String login, String password, String userId,
			Role role) {
		User newUser = new User();
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);
		newUser.setLogin(login);
		newUser.setPassword(password);
		newUser.setRole(role);
		if (userId == null || userId.isEmpty()) {
			userDao.create(newUser);
		} else {
			newUser.setId(Long.parseLong(userId));
			userDao.update(newUser);
		}
	}

	@Override
	public boolean update(Long id) {
		User userForUpdate = new User();
		userForUpdate.setId(id);
		userDao.update(userForUpdate);
		return false;
	}

	@Override
	public User findByLoginName(String login) {
		return userDao.findByLoginName(login);
	}

	public IUserDAO getUserDao() {
		return userDao;
	}

	public void setUserDao(IUserDAO userDao) {
		this.userDao = userDao;
	}
}
