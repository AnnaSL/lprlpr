package com.nixsolutions.lesovaya.services.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.nixsolutions.lesovaya.controller.AuthorController;
import com.nixsolutions.lesovaya.dao.IAuthorDAO;
import com.nixsolutions.lesovaya.entity.Author;
import com.nixsolutions.lesovaya.services.IAuthorService;

//@Transactional
public class AuthorService implements IAuthorService{
	@Autowired
	@Qualifier("iAuthorDao")
	private IAuthorDAO authorDao;
	private static Logger log = LogManager.getLogger(AuthorService.class.getName());
	
	public boolean delete(Long id) {
		boolean result = false;
		Author author = authorDao.findById(id);
		if (author.getBooks().isEmpty()) {
			author.setId(id);
			authorDao.delete(author);
			result = true;
		}
		return result;
	}

	public List<Author> findAll() {
		return authorDao.findAll();
	}

	public Author findById(Long id) {
		return authorDao.findById(id);
	}

	public void save(Author author) {
		if (author.getId() == null ) {
			authorDao.create(author);
		} else {
			authorDao.update(author);
		}		
	}

	public List<Author> findByBookId(Long bookid) {		
//		return authorDao.findByBookId(bookid);
		return null;
	}

}
