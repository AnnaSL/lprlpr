package com.nixsolutions.lesovaya.services.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.nixsolutions.lesovaya.dao.IAuthorDAO;
import com.nixsolutions.lesovaya.dao.IBookCategoryDAO;
import com.nixsolutions.lesovaya.dao.IBookCellDAO;
import com.nixsolutions.lesovaya.dao.IBookClientDAO;
import com.nixsolutions.lesovaya.dao.IBookDAO;
import com.nixsolutions.lesovaya.entity.Author;
import com.nixsolutions.lesovaya.entity.Book;
import com.nixsolutions.lesovaya.entity.BookCell;
import com.nixsolutions.lesovaya.services.IBookService;

//@Transactional
public class BookService implements IBookService {
	@Autowired
	@Qualifier("iBookDao")
	private IBookDAO bookDao;
	@Autowired
	@Qualifier("iBookCategoryDao")
	private IBookCategoryDAO bookCategoryDao;
	@Autowired
	@Qualifier("iAuthorDao")
	private IAuthorDAO authorDao;
	@Autowired
	@Qualifier("iBookCellDao")
	private IBookCellDAO cellDao;
	@Autowired
	@Qualifier("iBookClientDao")
	private IBookClientDAO clientDao;

	//must be a transaction
	public void save(String isbn, String title, String category, String description, String[] authorsIds, String bookid,
			String cellNum, String afterAction, String cellcheck, String curcellid) {
		Book book = new Book();
		book.setIsbn(isbn);
		book.setTitle(title);
		book.setCategory(bookCategoryDao.findById(Long.parseLong(category)));
		book.setDescription(description);
		BookCell cell = new BookCell();

		if (bookid == null || bookid.isEmpty()) {
				
				Set<Author> authors = new HashSet<Author>();
				for (String authorID: authorsIds) {								
					authors.add(authorDao.findById(Long.parseLong(authorID)));
				}
				book.setAuthors(authors);
				Long key = bookDao.create(book);
				cell = cellDao.findById(Long.parseLong(cellNum));
				cell.setBook(bookDao.findById(key));
				cellDao.update(cell);	
				
		} else {
			book.setId(Long.parseLong(bookid));										
			Set<Author> authorSetFromDB = bookDao.findById(Long.parseLong(bookid)).getAuthors();
			Set<Author> authorsUpdate = new HashSet<Author>();
			for (String authorID: authorsIds) {							
				authorsUpdate.add(authorDao.findById(Long.parseLong(authorID)));
			}
			
			String[] authorsIdFromDB = new String[authorSetFromDB.size()];
			int i = 0;
			
			for (Author a : authorSetFromDB) {
				authorsIdFromDB[i] = Long.toString(a.getId());
				i++;
			}
			if (!Arrays.equals(authorsIdFromDB, authorsIds)) {
				book.getAuthors().clear();
				bookDao.update(book);
				book.setAuthors(authorsUpdate);
				bookDao.update(book);
			} else {
				bookDao.update(book);
			}
			
			if (afterAction.equalsIgnoreCase("afterEdit")
					&& cellcheck != null) {
				cell = cellDao.findById(Long.parseLong(cellNum));
				cell.setBook(bookDao.findById(Long.parseLong(bookid)));
				cellDao.update(cell);
				cellDao.updateToEmpty(Long.parseLong(curcellid));
			}			
		}		
	}

	//must be a transaction
	public boolean delete(Long bookid) {
		boolean result = false;
		if (clientDao.findCountRentsByBook(bookid) == 0) {
			cellDao.updateToEmptyByBookId(bookid);
			Book bookForDeleting = bookDao.findById(bookid);
			bookDao.delete(bookForDeleting);
			result = true;
		}
		return result;
	}

	public Book findById(Long id) {		
		return bookDao.findById(id);
	}

	public IBookDAO getBookDao() {
		return bookDao;
	}

	public void setBookDao(IBookDAO bookDao) {
		this.bookDao = bookDao;
	}

	public IBookCategoryDAO getBookCategoryDao() {
		return bookCategoryDao;
	}

	public void setBookCategoryDao(IBookCategoryDAO bookCategoryDao) {
		this.bookCategoryDao = bookCategoryDao;
	}

	public IAuthorDAO getAuthorDao() {
		return authorDao;
	}

	public void setAuthorDao(IAuthorDAO authorDao) {
		this.authorDao = authorDao;
	}

	public IBookCellDAO getCellDao() {
		return cellDao;
	}

	public void setCellDao(IBookCellDAO cellDao) {
		this.cellDao = cellDao;
	}

	public IBookClientDAO getClientDao() {
		return clientDao;
	}

	public void setClientDao(IBookClientDAO clientDao) {
		this.clientDao = clientDao;
	}
	
	@Override
	public List<Book> findAll() {
		return bookDao.findAll();
	}

}
