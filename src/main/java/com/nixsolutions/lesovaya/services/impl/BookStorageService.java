package com.nixsolutions.lesovaya.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.nixsolutions.lesovaya.dao.IBookCellDAO;
import com.nixsolutions.lesovaya.entity.BookCell;
import com.nixsolutions.lesovaya.services.IBookStorageService;

//@Transactional
public class BookStorageService implements IBookStorageService {
	
	@Autowired
	@Qualifier("iBookCellDao")
	private IBookCellDAO bookCellDao;
	
	public List<Integer> findAllCases() {
		
		List<Integer> allCases = null;
		allCases = bookCellDao.findAllCases();
		return allCases;
	}

	public Integer findMaxCaseNumber() {
		Integer maxCasenumber = 0;
		maxCasenumber = bookCellDao.findMaxCaseNumber();
		return maxCasenumber;
	}

	public List<BookCell> findCellsByCaseNumOrFullness(String casenum, String fullness) throws NumberFormatException {
		List<BookCell> listBookCell = null;
		listBookCell = bookCellDao.findAllByCaseNumOrFullness(Integer.parseInt(casenum), fullness);
		return listBookCell;
	}

	public void createCase(String bookCaseNumber, String bookShelfNumber, String cellNumber) {

		int caseNum = Integer.parseInt(bookCaseNumber);
		int shelfNumber = Integer.parseInt(bookShelfNumber);
		int cellPerShelf = Integer.parseInt(cellNumber);
		BookCell cell = new BookCell();

		for (int i = 1; i <= shelfNumber; i++) {
			for (int j = 1; j <= cellPerShelf; j++) {
				cell.setBookCaseNumber(caseNum);
				cell.setBookShelfNumber(i);
				cell.setCellNumber(j);
				bookCellDao.create(cell);
			}
		}		
	}

	public boolean delete(String caseNum) {
		
		boolean result = false;
		Integer caseNumber = Integer.parseInt(caseNum);
		if (bookCellDao.findCountOfBusyCells(caseNumber) == 0) {
			bookCellDao.deleteCase(caseNumber);
			result = true;
		} else {
			result = false;
		}
		return result;
	}

	public IBookCellDAO getBookCellDao() {
		return bookCellDao;
	}

	public void setBookCellDao(IBookCellDAO bookCellDao) {
		this.bookCellDao = bookCellDao;
	}

	public List<BookCell> findAllEmpty() {
		return bookCellDao.findAllEmpty();
	}

	@Override
	public BookCell findByBookId(Long id) {
		return bookCellDao.findByBookId(id);
	}

}
