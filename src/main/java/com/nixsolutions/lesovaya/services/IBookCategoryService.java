package com.nixsolutions.lesovaya.services;

import java.util.List;

import com.nixsolutions.lesovaya.entity.BookCategory;

public interface IBookCategoryService {
	
	List<BookCategory> findAll();
	BookCategory findById(long id);
	void save(BookCategory category);
	boolean delete(Long categoryid);
}
