package com.nixsolutions.lesovaya.services;

public interface IBookClientService {
	Long findCountRentsByBook(Long bookid);
}
