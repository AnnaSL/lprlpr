package com.nixsolutions.lesovaya.services;

import java.util.List;

import com.nixsolutions.lesovaya.entity.BookCell;

public interface IBookStorageService {
	
	List<Integer> findAllCases();
	Integer findMaxCaseNumber();
	List<BookCell> findCellsByCaseNumOrFullness(String casenum, String fullness) throws NumberFormatException;
	void createCase(String bookCaseNumber, String bookShelfNumber, String cellNumber);
	boolean delete(String caseNum);
	List<BookCell> findAllEmpty();
	BookCell findByBookId(Long id);
	
}
