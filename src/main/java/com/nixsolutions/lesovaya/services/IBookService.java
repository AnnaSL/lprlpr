package com.nixsolutions.lesovaya.services;

import java.util.List;

import com.nixsolutions.lesovaya.entity.Book;

public interface IBookService {

	void save(String isbn, String title,String category, String description, String[] authorsIds, String bookid, String cellNum, String afterAction, String cellcheck, String curcellid) throws NumberFormatException;
	boolean delete(Long bookid);
	List<Book> findAll();
	Book findById(Long id);
}
