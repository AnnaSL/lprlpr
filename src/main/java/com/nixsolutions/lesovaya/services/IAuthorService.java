package com.nixsolutions.lesovaya.services;

import java.util.List;

import com.nixsolutions.lesovaya.entity.Author;

public interface IAuthorService {
	
	boolean delete(Long id);
	List<Author> findAll();
	Author findById(Long id);
	void save(Author author);
//	List<Author> findByBookId(Long bookid);

}
