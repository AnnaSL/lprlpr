package com.nixsolutions.lesovaya.dao;

import java.sql.SQLException;
import java.util.List;

import com.nixsolutions.lesovaya.entity.Client;

public interface IClientDAO {

	void create(Client client);
	void update(Client client);
	void delete(Client client);
	Client findById(Integer id);
	List<Client> findByClientLastName(String lastName);
	List<Client> findAll();
	List<Client> findClientsNotReturnedSomeBook();
}
