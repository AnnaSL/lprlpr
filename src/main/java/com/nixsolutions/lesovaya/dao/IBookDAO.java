package com.nixsolutions.lesovaya.dao;

import java.util.List;

import com.nixsolutions.lesovaya.entity.Book;

public interface IBookDAO {
	Long create(Book book);
	void update(Book book);
	void delete(Book book);
	Book findById(Long id);
	List<Book> findAll();
	List<Book> findByTitle(String title);
	Book findByIsbn(String isbn);
	Long findCountBooksByCategory(Long categoryid);
}
