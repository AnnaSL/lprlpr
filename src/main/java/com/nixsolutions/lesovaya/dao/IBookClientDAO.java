package com.nixsolutions.lesovaya.dao;

import java.sql.SQLException;
import java.util.List;

import com.nixsolutions.lesovaya.entity.BookClient;

public interface IBookClientDAO {
	void create(BookClient bookClient);
	void update(BookClient bookClient);
	void delete(BookClient bookClient);
	BookClient findById(Integer id);
	List<BookClient> findAll();
	Long findCountRentsByBook(Long bookid);
}
