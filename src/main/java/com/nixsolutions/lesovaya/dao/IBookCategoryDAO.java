package com.nixsolutions.lesovaya.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.nixsolutions.lesovaya.entity.BookCategory;;

public interface IBookCategoryDAO {
	void create(BookCategory category);
	void update(BookCategory category);
	void delete(BookCategory category);
	BookCategory findById(Long id);
	List<BookCategory> findAll();
}
