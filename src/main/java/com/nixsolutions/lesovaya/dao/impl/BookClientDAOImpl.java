package com.nixsolutions.lesovaya.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.nixsolutions.lesovaya.entity.BookClient;
import com.nixsolutions.lesovaya.dao.IBookClientDAO;

public class BookClientDAOImpl implements IBookClientDAO {

	private SessionFactory sessionFactory;

	@Autowired
	public BookClientDAOImpl(@Qualifier("sessionFactory") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long findCountRentsByBook(Long bookid) {
		Long count = 0L;
		Session session = sessionFactory.openSession();
		String hql = "select count(*) as count from BookClient bc WHERE bc.book.id = :bookid";
		try {
			Query query = session.createQuery(hql);
			query.setLong("bookid", bookid);
			query.setMaxResults(1);
			count = (Long) query.uniqueResult();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return count;
	}

	@Override
	public void create(BookClient bookClient) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(bookClient);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void update(BookClient bookClient) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(bookClient);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void delete(BookClient bookClient) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(bookClient);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public BookClient findById(Integer id) {
		Session session = sessionFactory.openSession();
		BookClient client = null;
		try {
			client = (BookClient) session.get(BookClient.class, id);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return client;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BookClient> findAll() {
		List<BookClient> clientList = null;
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from BookClient");
			clientList = query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return clientList;
	}
}
