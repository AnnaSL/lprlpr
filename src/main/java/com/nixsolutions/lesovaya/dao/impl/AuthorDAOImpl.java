package com.nixsolutions.lesovaya.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.nixsolutions.lesovaya.dao.IAuthorDAO;
import com.nixsolutions.lesovaya.entity.Author;

public class AuthorDAOImpl implements IAuthorDAO {

	private SessionFactory sessionFactory;

	@Autowired
	public AuthorDAOImpl(@Qualifier("sessionFactory") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void create(Author author) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(author);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void update(Author author) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(author);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Author author) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(author);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public Author findById(Long id) {
		Session session = sessionFactory.openSession();
		Author author = null;
		try {
			author = (Author) session.get(Author.class, id);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return author;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> findAll() {
		List<Author> authorList = null;
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from Author");
			authorList = (List<Author>) query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return authorList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> findByLastName(String lastName) {
		List<Author> authorList = null;
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from Author a where lower(a.lastName) = lower(:lastName)");
			query.setString("lastName", lastName.trim());
			authorList = (List<Author>) query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return authorList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> findByFirstLetter(String firstLetter) {
		List<Author> authorList = new ArrayList<Author>();
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from Author as a where upper(a.lastName) like upper(:firstLetter)");
			query.setString("firstLetter", firstLetter + "%");
			authorList = query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return authorList;
	}

}
