package com.nixsolutions.lesovaya.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.nixsolutions.lesovaya.dao.IUserDAO;
import com.nixsolutions.lesovaya.entity.User;

public class UserDAOImpl implements IUserDAO {

	private SessionFactory sessionFactory;
	private static Logger log = LogManager.getLogger(UserDAOImpl.class.getName());

	@Autowired
	public UserDAOImpl(@Qualifier("sessionFactory") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void create(User user) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(user);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void update(User user) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(user);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void delete(User user) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(user);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public User findUserById(Long id) {
		Session session = sessionFactory.openSession();
		User user = null;
		try {
			user = (User) session.get(User.class, id);
			session.flush();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findByName(String name) {
		List<User> userList = null;
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from User as user where user.lastName = :lastName");
			userList = query.setString("lastName", name).list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return userList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAll() {
		Session session = sessionFactory.getCurrentSession();
		List<User> userList = null;
		try {
			Query query = session.createQuery("from User");
			userList = query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return userList;
	}

	@Override
	public boolean findByLoginAndPassword(String login, String password) {
		boolean result = false;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(User.class);
			Criterion conditionLogin = Restrictions.eq("login", login);
			Criterion conditionPassword = Restrictions.eq("password", password);
			criteria.add(Restrictions.and(conditionLogin, conditionPassword));
			criteria.setMaxResults(1);
			if (criteria.uniqueResult() != null) {
				result = true;
			}
			tx.commit();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return result;
	}
	
	@Override
	public User findByLoginName(String name) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		User user = null;
		try {
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(User.class);
			criteria.setFetchMode("role", FetchMode.JOIN);
			Criterion conditionLogin = Restrictions.eq("login", name);
			criteria.add(conditionLogin);
			criteria.setMaxResults(1);
			user = (User) criteria.uniqueResult();
			tx.commit();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return user;
	}

}
