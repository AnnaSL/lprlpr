package com.nixsolutions.lesovaya.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.nixsolutions.lesovaya.dao.IBookCategoryDAO;
import com.nixsolutions.lesovaya.entity.BookCategory;

public class BookCategoryDAOImpl implements IBookCategoryDAO {

	private SessionFactory sessionFactory;

	@Autowired
	public BookCategoryDAOImpl(@Qualifier("sessionFactory") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void create(BookCategory category) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(category);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void update(BookCategory category) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(category);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}

	}

	@Override
	public void delete(BookCategory category) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(category);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public BookCategory findById(Long id) {
		Session session = sessionFactory.openSession();
		BookCategory category = null;
		try {
			category = (BookCategory) session.get(BookCategory.class, id);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return category;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BookCategory> findAll() {
		List<BookCategory> categoryList = null;
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from BookCategory");
			categoryList = (List<BookCategory>) query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return categoryList;
	}

}
