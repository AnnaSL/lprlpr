package com.nixsolutions.lesovaya.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.nixsolutions.lesovaya.dao.IBookCellDAO;
import com.nixsolutions.lesovaya.entity.BookCell;

public class BookCellDAOImpl implements IBookCellDAO {

	private SessionFactory sessionFactory;

	@Autowired
	public BookCellDAOImpl(@Qualifier("sessionFactory") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void create(BookCell cell) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(cell);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void update(BookCell cell) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(cell);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void delete(BookCell cell) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(cell);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public BookCell findById(Long id) {
		Session session = sessionFactory.openSession();
		BookCell cell = null;
		try {
			cell = (BookCell) session.get(BookCell.class, id);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return cell;
	}

	@Override
	public BookCell findByCellNum(Integer caseCell, Integer shelf, Integer cell) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		BookCell bookCell = null;
		try {
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(BookCell.class);
			Criterion conditionCase = Restrictions.eq("bookCaseNumber", caseCell);
			Criterion conditionShelf = Restrictions.eq("bookShelfNumber", shelf);
			Criterion conditionCell = Restrictions.eq("cellNumber", cell);
			criteria.add(Restrictions.and(conditionCase, conditionShelf, conditionCell));
			criteria.setMaxResults(1);
			bookCell = (BookCell) criteria.uniqueResult();
			tx.commit();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return bookCell;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BookCell> findAll() {
		List<BookCell> cellList = null;
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from BookCell");
			cellList = (List<BookCell>) query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return cellList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BookCell> findAllEmpty() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<BookCell> bookCellList = null;
		try {
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(BookCell.class);
			Criterion conditionBookNull = Restrictions.isNull("book");
			criteria.add(Restrictions.and(conditionBookNull));
			bookCellList = criteria.list();
			tx.commit();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return bookCellList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BookCell> findCellByBookTitle(String bookTitle) {
		List<BookCell> cellList = new ArrayList<BookCell>();
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery(
					"from BookCell as bc inner join bc.book as book where lower(book.title) = lower(:title)");
			query.setString("title", bookTitle);
			List<Object> result = (List<Object>) query.list();
			Iterator<Object> itr = result.iterator();
			while (itr.hasNext()) {
				Object[] obj = (Object[]) itr.next();
				cellList.add((BookCell) obj[0]);
			}
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return cellList;
	}

	@Override
	public void updateToEmpty(Long id) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			String sql = "UPDATE book_cell bc SET bc.book_id = null WHERE bc.book_cell_id  = :id";
			Query query = session.createSQLQuery(sql);
			query.setLong("id", id);
			query.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BookCell> findLastAdded() {
		List<BookCell> cellList = new ArrayList<BookCell>();
		Session session = sessionFactory.openSession();
		String hql = "from BookCell as bc where bc.bookCaseNumber = (select MAX(b.bookCaseNumber) from BookCell b)";
		try {
			Query query = session.createQuery(hql);
			cellList = query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return cellList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> findAllCases() {
		List<Integer> cellList = new ArrayList<Integer>();
		Session session = sessionFactory.openSession();
		String hql = "select distinct(bc.bookCaseNumber) as bookCaseNumber from BookCell bc";
		try {
			Query query = session.createQuery(hql);
			cellList = query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return cellList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BookCell> findAllByCaseNumOrFullness(int caseNum, String fullness) {
		List<BookCell> bookCellList = new ArrayList<BookCell>();
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(BookCell.class);
			if (caseNum != 0) {
				criteria.add(Restrictions.eq("bookCaseNumber", caseNum));
				if (fullness != null && fullness.equalsIgnoreCase("busy")) {
					criteria.add(Restrictions.isNotNull("book"));
				}
				if (fullness != null && fullness.equalsIgnoreCase("empty")) {
					criteria.add(Restrictions.isNull("book"));
				}
			} else {
				if (fullness != null && fullness.equalsIgnoreCase("busy")) {
					criteria.add(Restrictions.isNotNull("book"));
				}
				if (fullness != null && fullness.equalsIgnoreCase("empty")) {
					criteria.add(Restrictions.isNull("book"));
				}
			}
			bookCellList = criteria.list();
			tx.commit();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return bookCellList;
	}

	@Override
	public Integer findMaxCaseNumber() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Integer num = null;
		try {
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(BookCell.class);
			criteria.setProjection(Projections.max("bookCaseNumber"));
			criteria.setMaxResults(1);
			num = (Integer) criteria.uniqueResult();
			tx.commit();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return num + 1;
	}

	@Override
	public Long findCountOfBusyCells(int caseNumber) {
		Long count = 0L;
		Session session = sessionFactory.openSession();
		String hql = "select count(bc.id) as count from BookCell bc where bc.book is not null and bc.bookCaseNumber = :bookCaseNumber";
		try {
			Query query = session.createQuery(hql);
			query.setInteger("bookCaseNumber", caseNumber);
			query.setMaxResults(1);
			count = (Long) query.uniqueResult();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return count;
	}

	@Override
	public void deleteCase(int caseNumber) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			String hql = "delete from BookCell WHERE bookCaseNumber = :bookCaseNumber";
			Query query = session.createQuery(hql);
			query.setInteger("bookCaseNumber", caseNumber);
			query.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}

	}
	
	@Override
	public void updateToEmptyByBookId(Long id) {
		Session session = sessionFactory.openSession();
		try {
			String sql = "UPDATE book_cell bc SET bc.book_id = null WHERE bc.book_id  = :id";
			Query query = session.createSQLQuery(sql);
			query.setLong("id", id);
			query.executeUpdate();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}

	}

	@Override
	public BookCell findByBookId(Long bookId) {
		Session session = sessionFactory.openSession();
		BookCell cell = null;
		String hql = "select bc from BookCell as bc inner join bc.book as book where bc.book.id = :bookId";
		try {
			Query query = session.createQuery(hql);
			query.setLong("bookId", bookId);
			cell = (BookCell) query.uniqueResult();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return cell;
	}
}
