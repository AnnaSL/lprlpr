package com.nixsolutions.lesovaya.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.nixsolutions.lesovaya.dao.IRoleDAO;
import com.nixsolutions.lesovaya.entity.Role;

public class RoleDAOImpl implements IRoleDAO {

	private SessionFactory sessionFactory;
	private static Logger log = LogManager.getLogger(RoleDAOImpl.class.getName());

	@Autowired
	public RoleDAOImpl(@Qualifier("sessionFactory") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void create(Role role) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(role);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw new RuntimeException(e);
		} finally {
			session.flush();
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void update(Role role) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(role);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw new RuntimeException(e);
		} finally {
			session.flush();
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Role role) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(role);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw new RuntimeException(e);
		} finally {
			session.flush();
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public Role findRoleById(Long id) {
		Session session = sessionFactory.openSession();
		Role role = null;
		try {
			role = (Role) session.get(Role.class, id);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return role;
	}

	@Override
	public Role findByName(String name) {
		Session session = sessionFactory.openSession();
		Role role = new Role();
		try {
			Query query = session.createQuery("from Role where name = :name");
			query.setString("name", name);
			role = (Role) query.uniqueResult();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return role;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> findAll() {
		List<Role> listOfRoles = new ArrayList<Role>();
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from Role");
			listOfRoles = query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return listOfRoles;
	}
}
