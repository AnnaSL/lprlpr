package com.nixsolutions.lesovaya.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.nixsolutions.lesovaya.dao.IBookDAO;
import com.nixsolutions.lesovaya.entity.Book;

public class BookDAOImpl implements IBookDAO {

	private SessionFactory sessionFactory;

	@Autowired
	public BookDAOImpl(@Qualifier("sessionFactory") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long create(Book book) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Long key = 0l;
		try {
			tx = session.beginTransaction();
			key = (Long) session.save(book);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return key;
	}

	@Override
	public void update(Book book) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(book);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}

	}

	@Override
	public void delete(Book book) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(book);
			;
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}

	}

	@Override
	public Book findById(Long id) {
		Session session = sessionFactory.openSession();
		Book book = null;
		try {
			book = (Book) session.get(Book.class, id);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return book;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> findAll() {
		List<Book> bookList = null;
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from Book");
			bookList = query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return bookList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> findByTitle(String title) {
		List<Book> bookList = null;
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from Book as book where lower(book.title) = lower(:title)");
			query.setString("title", title.trim());
			bookList = query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return bookList;
	}

	@Override
	public Book findByIsbn(String isbn) {
		Book book = null;
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from Book as book where book.isbn = :isbn");
			query.setString("isbn", isbn.trim());
			query.setMaxResults(1);
			book = (Book) query.uniqueResult();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return book;
	}

	@Override
	public Long findCountBooksByCategory(Long categoryid) {
		Long count = 0L;
		Session session = sessionFactory.openSession();
		String hql = "select count(book.id) from Book as book where book.category.id = :categoryId";
		try {
			Query query = session.createQuery(hql);
			query.setLong("categoryId", categoryid);
			query.setMaxResults(1);
			count = (Long) query.uniqueResult();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return count;
	}

}
