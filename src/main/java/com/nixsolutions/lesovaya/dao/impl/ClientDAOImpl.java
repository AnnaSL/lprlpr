package com.nixsolutions.lesovaya.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.nixsolutions.lesovaya.dao.IClientDAO;
import com.nixsolutions.lesovaya.entity.Client;

public class ClientDAOImpl implements IClientDAO {

	private SessionFactory sessionFactory;

	@Autowired
	public ClientDAOImpl(@Qualifier("sessionFactory") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void create(Client client) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(client);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}		
	}

	@Override
	public void update(Client client) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(client);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}		
	}

	@Override
	public void delete(Client client) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(client);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}		
	}

	@Override
	public Client findById(Integer id) {
		Session session = sessionFactory.openSession();
		Client client = null;
		try {
			client = (Client) session.get(Client.class, id);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return client;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Client> findByClientLastName(String lastName) {
		List<Client> clientList = null;
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from Client c where lower(c.lastName) = lower(:lastName)");
			query.setString("lastName", lastName.trim());
			clientList = query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return clientList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Client> findAll() {
		List<Client> clientList = null;
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("from Client");
			clientList = query.list();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return clientList;
	}

	@Override
	public List<Client> findClientsNotReturnedSomeBook() {
		// TODO Auto-generated method stub
		return null;
	}
}
