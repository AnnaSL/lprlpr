package com.nixsolutions.lesovaya.dao;

import java.util.List;

import com.nixsolutions.lesovaya.entity.User;

public interface IUserDAO {
	void create(User user);
	void update(User user);
	void delete(User user);
	User findUserById(Long id);
	List<User> findByName(String name);
	User findByLoginName(String name);
	List<User> findAll();
	boolean findByLoginAndPassword(String login, String password);
}
