package com.nixsolutions.lesovaya.dao;

import java.util.List;

import com.nixsolutions.lesovaya.entity.Author;

public interface IAuthorDAO {
	void create(Author author);
	void update(Author author);
	void delete(Author author);
	Author findById(Long id);
	List<Author> findAll();
	List<Author> findByLastName(String lastName);
//	List<Author> findByBookId(Long id);
	List<Author> findByFirstLetter(String firstLetter);
}
