package com.nixsolutions.lesovaya.dao;

import java.util.List;

import com.nixsolutions.lesovaya.entity.Role;

public interface IRoleDAO {
	void create(Role role);
	void update(Role role);
	void delete(Role role);
	Role findRoleById(Long id);
	Role findByName(String name);
	List<Role> findAll();
}
