package com.nixsolutions.lesovaya.dao;

import java.sql.SQLException;
import java.util.List;

import com.nixsolutions.lesovaya.entity.BookCell;

public interface IBookCellDAO {
	void create(BookCell cell);
	void update(BookCell cell);
	void delete(BookCell cell);
	BookCell findById(Long id);
	BookCell findByCellNum(Integer casecell, Integer shelf, Integer cell);
	List<BookCell> findAll();
	List<BookCell> findAllEmpty();
	List<BookCell> findCellByBookTitle(String bookTitle);
	void updateToEmpty(Long id);
	List<BookCell> findLastAdded();
	List<Integer> findAllCases();
	List<BookCell> findAllByCaseNumOrFullness(int caseNum, String fullness);
	Integer findMaxCaseNumber();
	Long findCountOfBusyCells(int caseNumber);
	void deleteCase(int caseNumber);
	void updateToEmptyByBookId(Long id);
	BookCell findByBookId(Long bookId);
}
