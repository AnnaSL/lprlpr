package com.nixsolutions.lesovaya.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nixsolutions.lesovaya.entity.BookCategory;
import com.nixsolutions.lesovaya.services.IBookCategoryService;

@Controller
public class CategoryController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger log = LogManager.getLogger(CategoryController.class.getName());
	@Autowired(required=true)
	@Qualifier(value="iBookCategoryService")
	private IBookCategoryService bookCategoryService; 
	
	@RequestMapping(value = "/category/list", method = RequestMethod.GET)
	public String openListCategory(Model model) {
		model.addAttribute("categories", this.bookCategoryService.findAll());
		return "listCategory";
	}
	
	@RequestMapping(value = "/category/edit", method = RequestMethod.GET)
	public String editCategory(@RequestParam("categoryid") Long categoryId, Model model) {
		model.addAttribute("category", this.bookCategoryService.findById(categoryId));
		return "category";
	}
	
	@RequestMapping(value = "/category/delete", method = RequestMethod.POST)
	public String deleteCategory(@RequestParam("categoryid") Long categoryId, Model model) {
		if (!this.bookCategoryService.delete(categoryId)) {
			model.addAttribute("categoryAssociated", 1);
		}
		model.addAttribute("categories", this.bookCategoryService.findAll());
		return "listCategory";
	}
	
	@RequestMapping(value = "/category/add", method = RequestMethod.GET)
	public String addCategory(Model model) {
		model.addAttribute("category", new BookCategory());
		return "category";
	}
	
	@RequestMapping(value = "/category/save", method = RequestMethod.POST)
	public String saveCategory(@ModelAttribute("category") BookCategory category, Model model) {		
		this.bookCategoryService.save(category);
		model.addAttribute("categories", this.bookCategoryService.findAll());
		return "listCategory";
	}
}
