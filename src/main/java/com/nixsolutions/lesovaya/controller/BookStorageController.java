package com.nixsolutions.lesovaya.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nixsolutions.lesovaya.entity.BookCell;
import com.nixsolutions.lesovaya.services.IBookStorageService;

@Controller
public class BookStorageController {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger log = LogManager.getLogger(BookStorageController.class.getName());
	@Autowired(required=true)
	@Qualifier(value="iBookStorageService")	
	private IBookStorageService bookStorageService;

	@RequestMapping(value = "/case/list", method = RequestMethod.GET)
	public String openListCases(Model model) {
		model.addAttribute("cases", this.bookStorageService.findAllCases());
		model.addAttribute("case", new BookCell());
		return "listCases";
	}
	
	@RequestMapping(value = "/case/view", method = RequestMethod.GET)
	public String viewCellsOfCase(@RequestParam("casenum") String caseNum, Model model) {
		model.addAttribute("storage", this.bookStorageService.findCellsByCaseNumOrFullness(caseNum, "empty"));
		model.addAttribute("caseNum", caseNum);
		return "listStorage";
	}
	
	@RequestMapping(value = "/case/delete", method = RequestMethod.POST)
	public String deleteCase(@RequestParam("casenum") String caseNum, Model model) {
		if (!bookStorageService.delete(caseNum)) {
			model.addAttribute("caseNotEmpty", 1);
		}
		model.addAttribute("cases", this.bookStorageService.findAllCases());
		return "listCases";
	}
	
	@RequestMapping(value = "/case/add", method = RequestMethod.GET)
	public String addCase(Model model) throws Exception {
		model.addAttribute("maxcasenum", this.bookStorageService.findMaxCaseNumber());
		return "storage";
	}
	
	@RequestMapping(value = "/case/filterCells", method = RequestMethod.GET)
	public String filterCell(@RequestParam("fullness") String fullness,@RequestParam("casenumFilter") String casenumFilter, Model model) {
		model.addAttribute("caselist", this.bookStorageService.findAllCases());
		model.addAttribute("caseNum", casenumFilter);
		model.addAttribute("storage", this.bookStorageService.findCellsByCaseNumOrFullness(casenumFilter, fullness));
		return "listStorage";
	}
	
	@RequestMapping(value = "/case/save", method = RequestMethod.POST)
	public String saveCase(@RequestParam("bookCaseNumber") String bookCaseNumber,@RequestParam("bookShelfNumber") String bookShelfNumber,@RequestParam("cellNumber") String cellNumber, Model model) {
		
		this.bookStorageService.createCase(bookCaseNumber, bookShelfNumber, cellNumber);
		model.addAttribute("cases", this.bookStorageService.findAllCases());
		return "listCases";
	}

	public IBookStorageService getBookStorageService() {
		return bookStorageService;
	}

	public void setBookStorageService(IBookStorageService bookStorageService) {
		this.bookStorageService = bookStorageService;
	}
}
