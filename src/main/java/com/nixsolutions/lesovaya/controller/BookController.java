package com.nixsolutions.lesovaya.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nixsolutions.lesovaya.entity.Book;
import com.nixsolutions.lesovaya.services.IAuthorService;
import com.nixsolutions.lesovaya.services.IBookCategoryService;
import com.nixsolutions.lesovaya.services.IBookService;
import com.nixsolutions.lesovaya.services.IBookStorageService;

@Controller
public class BookController {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger log = LogManager.getLogger(BookController.class.getName());
	@Autowired
	@Qualifier("iBookService")
	private IBookService bookService;
	@Autowired
	@Qualifier("iBookStorageService")
	private IBookStorageService bookStorageService;
	@Autowired
	@Qualifier("iBookCategoryService")
	private IBookCategoryService bookCategoryService;
	@Autowired
	@Qualifier("iAuthorService")
	private IAuthorService authorService;

	
	@RequestMapping(value = "/book/list", method = RequestMethod.GET) 
	public String openListBook(Model model){
		model.addAttribute("books", this.bookService.findAll());
		return "listBooks";
	}
	
	@RequestMapping(value = "/book/edit", method = RequestMethod.GET)
	public String editBook(@RequestParam("bookid") Long bookId, @RequestParam("categoryid") Long categoryId, Model model) throws Exception {
		model.addAttribute("book", this.bookService.findById(bookId));		
		model.addAttribute("categorySelected", categoryId);
		model.addAttribute("cellNumList", bookStorageService.findAllEmpty());
		model.addAttribute("categoryList", bookCategoryService.findAll());
		model.addAttribute("authorList", authorService.findAll());
		model.addAttribute("bookCell", bookStorageService.findByBookId(bookId));
		model.addAttribute("editbook", "1");			
		return "book";
	}
	
	@RequestMapping(value = "/book/delete", method = RequestMethod.POST)
	public String deleteBook(@RequestParam("bookid") Long bookId, Model model) {
		if (!this.bookService.delete(bookId)) {
			model.addAttribute("rent", "1");
		}
		model.addAttribute("books", this.bookService.findAll());
		return "listBooks";
	}
	
	@RequestMapping(value = "/book/add", method = RequestMethod.GET)
	public String addBook(Model model) throws Exception {
		model.addAttribute("cellNumList", this.bookStorageService.findAllEmpty());
		model.addAttribute("categoryList", this.bookCategoryService.findAll());
		model.addAttribute("authorList", this.authorService.findAll());
		model.addAttribute("book", new Book());
		return "book";
	}
	
	@RequestMapping(value = "/book/save", method = RequestMethod.POST)
	public String saveBook(@ModelAttribute("isbn") String isbn, @ModelAttribute("title") String title,
			@ModelAttribute("category") String category, @ModelAttribute("description") String description,
			@ModelAttribute("authorsIds") String[] authorsIds, @ModelAttribute("cellNum") String cellNum,
			@ModelAttribute("afterAction") String afterAction, @ModelAttribute("cellcheck") String cellcheck,
			@ModelAttribute("curcellid") String curcellid, @ModelAttribute("bookid") String bookid, Model model)
					throws Exception {

		this.bookService.save(isbn, title, category, description, authorsIds, bookid, cellNum, afterAction, cellcheck,
				curcellid);
		model.addAttribute("books", this.bookService.findAll());
		return "listBooks";
	}
}
