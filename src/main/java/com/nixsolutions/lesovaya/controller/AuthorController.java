package com.nixsolutions.lesovaya.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nixsolutions.lesovaya.entity.Author;
import com.nixsolutions.lesovaya.services.IAuthorService;

@Controller
public class AuthorController {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger log = LogManager.getLogger(AuthorController.class.getName());
	@Autowired
	@Qualifier("iAuthorService")
	private IAuthorService authorService;
	
	@RequestMapping(value = "/author/list", method = RequestMethod.GET)
	public String openListAuthor(Model model) {
		model.addAttribute("authors", this.authorService.findAll());
		return "listAuthors";
	}
	
	@RequestMapping(value = "/author/edit", method = RequestMethod.GET)
	public String editAuthor(@RequestParam("authorid") Long authorId, Model model) {
		model.addAttribute("author", this.authorService.findById(authorId));
		return "author";
	}
	
	@RequestMapping(value = "/author/delete", method = RequestMethod.POST)
	public String deleteAuthor(@RequestParam("authorid") Long authorId, Model model) {
		if (!this.authorService.delete(authorId)) {
			model.addAttribute("bookAssociated", 1);
		}
		model.addAttribute("authors", this.authorService.findAll());
		return "listAuthors";
	}
	
	@RequestMapping(value = "/author/add", method = RequestMethod.GET)
	public String addAuthor(Model model) throws Exception {
		model.addAttribute("author", new Author());
		return "author";
	}
	
	@RequestMapping(value = "/author/save", method = RequestMethod.POST)
	public String saveAuthor(@ModelAttribute("author") Author author, Model model) throws Exception {		
		this.authorService.save(author);;
		model.addAttribute("authors", this.authorService.findAll());
		return "listAuthors";
	}
}
