package com.nixsolutions.lesovaya.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.nixsolutions.lesovaya.entity.Author;
import com.nixsolutions.lesovaya.entity.BookCategory;
import com.nixsolutions.lesovaya.services.IAuthorService;
import com.nixsolutions.lesovaya.services.IBookCategoryService;

@Component
@Path("/category")
public class AuthorResource {
	
	@Autowired(required=true)
	@Qualifier(value="iBookCategoryService")
	private IBookCategoryService bookCategoryService; 
	
	@GET
	@Path("/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public BookCategory getMsg(@PathParam("param") String msg) {
		BookCategory category = bookCategoryService.findById(Integer.parseInt(msg));
		
		return category; 
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String addCategory(@QueryParam("categoryName") String name) {
		BookCategory category = new BookCategory();
		category.setName(name);
		bookCategoryService.save(category);
	    return null;
	  }

	 @PUT
	 @Produces(MediaType.APPLICATION_JSON)
	 public String updateCategory(@QueryParam("categoryName") String name, @QueryParam("id") String id) {
		 BookCategory category = new BookCategory();
		 category.setName(name);
		 category.setId(Long.parseLong(id));
		 bookCategoryService.save(category);
		 return null;
	 }

	  @DELETE
	  @Produces(MediaType.APPLICATION_JSON)
	  public boolean deleteCategory(@QueryParam("id") Long id) {
	    return bookCategoryService.delete(id);
	  }

}
