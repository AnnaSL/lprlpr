package com.nixsolutions.lesovaya.controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.nixsolutions.lesovaya.entity.User;
import com.nixsolutions.lesovaya.services.IUserService;
import com.nixsolutions.lesovaya.services.impl.UserService;

@Controller
public class UserController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger log = LogManager.getLogger(UserController.class.getName());

	@Autowired(required = true)
	@Qualifier(value = "iUserService")
	private IUserService userService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String openWelcomePage(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, Model model) {
		User user = new User();
		model.addAttribute("user", user);
		if (error != null) {
			model.addAttribute("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addAttribute("msg", "You've been logged out successfully.");
		}
		return "welcome";
	}

	@RequestMapping(value = "/homePage", method = RequestMethod.GET)
	public String redirectToHomePage(Model model, SecurityContextHolderAwareRequestWrapper request) {
		String page = "";
		if (request.isUserInRole("ROLE_ADMIN")) {
			page = "menu";
		} else if (request.isUserInRole("ROLE_LIBRARIAN")) {
			page = "librarian";
		} else if (request.isUserInRole("ROLE_CLIENT")) {
			page = "client";
		}
		return page;
	}
	
	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = (UserService) userService;
	}
}
