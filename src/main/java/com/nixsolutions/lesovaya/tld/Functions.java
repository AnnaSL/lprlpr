package com.nixsolutions.lesovaya.tld;

import java.util.Collection;

public class Functions {
	private Functions(){}
	
	public static boolean contains(Collection<Object> collection, Object item) {
		return collection.contains(item);
	}
}
